import React from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const AccountProfileDetails = (props) => {
  const classes = useStyles();

  const [id, setId] = React.useState('');
  const [firstName, setFirstName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [phone, setPhone] = React.useState('');
  const [state, setState] = React.useState('');
  const [country, setCountry] = React.useState('');
  const [checked, setChecked] = React.useState(false);

  const [dataSource, setDataSource] = React.useState([]);

  const printall = () => {
    const a = dataSource;
    const obj = {
      id,
      name: firstName,
      lname: lastName,
      mail: email,
      phone,
      country,
      state,
      checked
    };
    console.log({ a, obj });
    const newInstance = [...a, obj];
    setDataSource(newInstance);
  };

  const handleChangeCheck = (value, rowId) => {
    dataSource[rowId - 1].checked = value;
    setChecked(value);
    console.log(dataSource);
  };

  const deleteElement = () => {
    let a = 0;
    for (let i = 0; i < dataSource.length; i++) {
      if (dataSource[i].checked === true) {
        a += 1;
        dataSource.splice(i, 1);
        setId('');
        setFirstName('');
        setLastName('');
        setEmail('');
        setPhone('');
        setState('');
        setCountry('');
        setChecked(false);
        console.log(dataSource);
      }

      if (a > 0 && i < dataSource.length) {
        dataSource[i].id -= 1;
        i--;
      }
    }
  };

  return (
    <form
      autoComplete="off"
      noValidate
      {...props}
    >
      <Card>
        <CardHeader
          subheader="The information can be edited"
          title="Profile"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                helperText="Please specify the id"
                label="Id"
                name="Id"
                value={id}
                onChange={(e) => {
                  setId(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="First name"
                name="firstName"
                value={firstName}
                onChange={(e) => {
                  setFirstName(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Last name"
                name="lastName"
                value={lastName}
                onChange={(e) => {
                  setLastName(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Email Address"
                name="email"
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Phone Number"
                name="phone"
                value={phone}
                onChange={(e) => {
                  setPhone(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Country"
                name="country"
                value={country}
                onChange={(e) => {
                  setCountry(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Select State"
                name="state"
                value={state}
                onChange={(e) => {
                  setState(e.target.value);
                }}
                required
                variant="outlined"
              />
            </Grid>
          </Grid>

        </CardContent>
        <Divider />
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            p: 2
          }}
        >
          <Button
            onClick={printall}
            color="primary"
            variant="contained"
          >
            Save details
          </Button>
        </Box>
      </Card>
      <Card>
        <CardHeader
          title="Profile Info Table"
        />
      </Card>
      <Grid
        item
        lg={12}
        md={12}
        xs={12}
      >
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">Id</TableCell>
                <TableCell align="center">First Name</TableCell>
                <TableCell align="center">Last Name</TableCell>
                <TableCell align="center">Email</TableCell>
                <TableCell align="center">Phone</TableCell>
                <TableCell align="center">Country</TableCell>
                <TableCell align="center">State</TableCell>
                <TableCell align="center">Delete Check</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {dataSource.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="center">{row.id}</TableCell>
                  <TableCell align="center">{row.name}</TableCell>
                  <TableCell align="center">{row.lname}</TableCell>
                  <TableCell align="center">{row.mail}</TableCell>
                  <TableCell align="center">{row.phone}</TableCell>
                  <TableCell align="center">{row.country}</TableCell>
                  <TableCell align="center">{row.state}</TableCell>
                  <TableCell align="center">
                    <Checkbox
                      id={row.id}
                      checked={row.checked}
                      onChange={() => {
                        handleChangeCheck(!(row.checked), row.id);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>

          <Divider />
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'flex-end',
              p: 2
            }}
          >
            <Button
              onClick={() => { deleteElement(); }}
              color="primary"
              variant="contained"
            >
              Delete Selected
            </Button>
          </Box>
        </TableContainer>
      </Grid>
    </form>
  );
};

export default AccountProfileDetails;
